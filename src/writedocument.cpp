#include <eosio/eosio.hpp>

using namespace eosio;

/**
 * @defgroup writedocumentcontract writedocument contract
 * @brief Defines the contract for writing and retrieving documents on the blockchain
 * @ingroup contracts
 */

/**
 * @defgroup writedocumentactions writedocument Actions
 * @ingroup writedocumentcontract
 * @brief Defines the actions available on the writedocument contract
 */

/**
 * @defgroup writedocumenttables writedocument Tables
 * @ingroup writedocumentcontract
 * @brief Defines the multi-index tables used in the writedocument contract
 */

/**
 * @defgroup writedocumentapi writedocument API
 * @ingroup writedocumentcontract
 * @brief Defines the public API available on the writedocument contract
 */

class [[eosio::contract("writedocument")]] writedocument : public eosio::contract {
  public:
    using contract::contract;

    /**
     * @defgroup write write action
     * @ingroup writedocumentactions
     * 
     * @brief Writes a new document to the blockchain
     * 
     * @param document The text string of the document to write
     * 
     * @pre Requires the authorization of the contract account
     * @post Stores the new document on the blockchain
     */
    [[eosio::action]]
    void write(const std::string& document);

    /**
     * @defgroup get_document get_document action
     * @ingroup writedocumentactions
     * 
     * @brief Retrieves the most recent document from the blockchain
     * 
     * @return The text string of the most recent document, or an empty string if no documents have been written
     * 
     * @pre None
     * @post None
     */
    [[eosio::action]]
    std::string get_document() const;

  private:
    /**
     * @defgroup documentstruct document struct
     * @ingroup writedocumenttables
     * 
     * @brief Defines the structure of the document table
     */
    struct [[eosio::table]] document {
        std::string text; ///< The text of the document
        uint64_t primary_key() const { return 0; }
    };

    /**
     * @defgroup documents_table documents table
     * @ingroup writedocumenttables
     * 
     * @brief Defines the multi-index table for storing documents on the blockchain
     */
    typedef eosio::multi_index<"documents"_n, document> documents_table;
};

EOSIO_DISPATCH(writedocument, (write)(get_document))
