#include <eosio/testing.hpp>
#include <eosio/transaction.hpp>
#include <eosio/crypto.hpp>
#include <writedocument.hpp>

using namespace eosio::testing;

/**
 * @brief Tests the write action of the writedocument contract
 */
TEST_CASE("write_action_test", "[writedocument]") {
    // Create a test harness for the writedocument contract
    writedocument tester = setup_contract<writedocument>();

    // Write a document to the blockchain
    std::string document_text = "This is a test document";
    tester.push_action("write"_n, std::vector<eosio::permission_level>{{tester.get_self(), "active"_n}}, {document_text});

    // Retrieve the most recent document from the blockchain
    std::string retrieved_document_text = tester.get_document();
    REQUIRE(retrieved_document_text == document_text);

    // Write a new document to the blockchain
    std::string new_document_text = "This is a new test document";
    tester.push_action("write"_n, std::vector<eosio::permission_level>{{tester.get_self(), "active"_n}}, {new_document_text});

    // Retrieve the most recent document from the blockchain
    std::string updated_document_text = tester.get_document();
    REQUIRE(updated_document_text == new_document_text);
}

/**
 * @brief Tests the get_document action of the writedocument contract when no documents have been written
 */
TEST_CASE("get_document_empty_test", "[writedocument]") {
    // Create a test harness for the writedocument contract
    writedocument tester = setup_contract<writedocument>();

    // Retrieve the most recent document from the blockchain
    std::string retrieved_document_text = tester.get_document();
    REQUIRE(retrieved_document_text == "");
}
