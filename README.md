# WriteDocument AntelopeIO Smart Contract

The `WriteDocument` smart contract is a simple AntelopeIO smart contract that allows you to write a single document to the blockchain and retrieve it at any time. The contract uses the AntelopeIO multi_index table to store the most recent document text as a `document` entry on the table. 

The original intended use for this contract is to record and retrieve governance documents to the `gov.libre` account of the Libre Blockchain.

## Usage

To use the `WriteDocument` smart contract, you will need to have access to an AntelopeIO blockchain and the `cleos` command-line interface. 

### Deployment

To deploy the `WriteDocument` smart contract, follow these steps:

1. Clone this repository and navigate to the `writedocument` directory:

```
git clone https://gitlab.com/DouglasHorn/write-document-antelope-smart-contract/
cd write-document-antelope-smart-contract/
```

2. Build the contract by running the following command:

```
mkdir build
cd build
cmake ..
make
```

3. Deploy the contract by running the following command:

```
cleos set contract <account_name> ../writedocument -p <account_name>@active
```

Replace `<account_name>` with the name of the account that you want to deploy the contract to.

### Writing a Document

To write a document using the `WriteDocument` smart contract, follow these steps:

1. Call the `write` action on the contract with the text of the document as the parameter:

```
cleos push action <contract_account> write '["<document_text>"]' -p <contract_account>@active
```

Replace `<contract_account>` with the name of the account that deployed the contract and `<document_text>` with the text of the document you want to write.

### Retrieving a Document

To retrieve the most recent document using the `WriteDocument` smart contract, follow these steps:

1. Call the `get_document` action on the contract:

```
cleos push action <contract_account> get_document '[]' -p <contract_account>@active
```

Replace `<contract_account>` with the name of the account that deployed the contract.

## Contributing

Contributions are welcome! If you would like to contribute to the `WriteDocument` smart contract, please create a pull request with your proposed changes.

## License

The `WriteDocument` smart contract is licensed under the MIT License. See `LICENSE` for more information.
