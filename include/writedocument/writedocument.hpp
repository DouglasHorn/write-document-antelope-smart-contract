#pragma once

#include <eosio/eosio.hpp>

using namespace eosio;

/**
 * @defgroup writedocumentcontract writedocument contract
 * @brief Defines the contract for writing and retrieving documents on the blockchain
 * @ingroup contracts
 */

/**
 * @defgroup writedocumentactions writedocument Actions
 * @ingroup writedocumentcontract
 * @brief Defines the actions available on the writedocument contract
 */

/**
 * @defgroup writedocumenttables writedocument Tables
 * @ingroup writedocumentcontract
 * @brief Defines the multi-index tables used in the writedocument contract
 */

/**
 * @defgroup writedocumentapi writedocument API
 * @ingroup writedocumentcontract
 * @brief Defines the public API available on the writedocument contract
 */

class [[eosio::contract("writedocument")
